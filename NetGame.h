#ifndef NETGAME_H
#define NETGAME_H

#include "Board.h"
#include <QTcpServer>
#include <QTcpSocket>

/*  可能给对方发出的消息
    1) 执红方还是黑方，这个信息有服务器发出，客户端接收
    第一个字节固定是1，第二个字节是1，或者0       1表示接收方走红旗，0表示走黑棋
    2）点击信息
    第一个字节固定是2，第二个字节是row，第三个字节是col，第四个字节是点击的棋子id
*/
class NetGame : public Board
{
    Q_OBJECT
public:
    NetGame(bool server);   //构造函数里要有变量
    ~NetGame();

    QTcpServer* _server;    //
    QTcpSocket* _socket;

    void click(int id, int row, int col);//重载虚函数，使得服务器走一步，客户端跟着走一步

public slots:
    void slotNewConnection();//新建槽函数
    void slotRecv();

};

#endif // NETGAME_H
