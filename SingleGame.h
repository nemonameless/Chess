#ifndef SINGLEGAME_H
#define SINGLEGAME_H

#include "Board.h"

class SingleGame : public Board
{
    Q_OBJECT
public:
    SingleGame()
    {
        _level = 6;
    }
    virtual void click(int id, int row, int col);  //virtual

    Step* getBestMove();
    void getAllPossibleMove(QVector<Step*>& steps);

    void fakeMove(Step* step);
    void unfakeMove(Step* step);
    int calcScore();

    int getMinScore(int level, int curMaxScore);
    int getMaxScore(int level, int curMinScore);

    int _level;

public slots:
    void computerMove();//添加槽函数开头要加Q_OBJECT，并且重新编译，且在workspace里删掉build chess....文件夹

};

#endif // SINGLEGAME_H
