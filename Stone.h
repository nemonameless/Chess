#ifndef STONE_H
#define STONE_H

#include <QRect>
#include <QPainter>

class Stone
{
public:
    Stone();
    ~Stone();

    enum TYPE{CHE, MA, PAO, BING, JIANG, SHI, XIANG};

    void init(int id);//初始化棋子，画出棋子来

    int _row;
    int _col;
    TYPE _type;
    bool _dead;
    bool _red;
    int _id;

    QString name();//根据棋子的类型返回中文名字

    void rotate();
};

#endif // STONE_H
